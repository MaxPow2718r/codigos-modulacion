import numpy as np
from matplotlib import pyplot as plt

fs=30000

f=500
m = [0.5,1,1.5]
T = 1.0 / f
t = np.linspace(0.0, 4*T, fs)
y = np.sin(f * 2.0*np.pi*t)

plt.plot(t,y)
plt.title("Señal Original")
plt.show()

Ac = 1
fportadora=10000 #10k
yportadora=Ac*np.cos(fportadora*2*np.pi*t)
salida = []
for i in m:
    salida.append(Ac*(1+i*y)*yportadora)


for i in salida:
    plt.plot(t,i)
    plt.show()

