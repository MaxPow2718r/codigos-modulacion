import numpy as np
from matplotlib import pyplot as plt
import Operadores
from scipy import signal

#frecuencia de muestreo
fs=900000
dt = 1 / fs

#frecuencia de la senal modulada
f = 10
T = 1 / f         #periodo
t = np.arange(0, 4*T, dt)
n = len(t)
y = np.sin(f * 2.0*np.pi*t)
ytongo = np.fft.fft(y,n)
freq = (1 / (dt * n)) * np.arange(n)
mag = ytongo * np.conj(ytongo) / n
L = np.arange(1, np.floor(n/2),dtype = 'int')

plt.plot(t,y)
plt.title("Señal Original")
plt.show()

plt.plot(freq[L], mag[L])
plt.title("Espectro de frecuencia de la señal original")
plt.xlim(0,20)
plt.xlabel("Frecuencia $[Hz]$")
plt.show()

#Modulacion DSB-SC a 10k
freq2 = (1 / (dt * n)) * np.arange(n) / 1000
fportadora = 200*f
yportadora = np.cos(fportadora*2*np.pi*t)
ymodulada = y * yportadora
yf = np.fft.fft(ymodulada,n)
mag2 = yf * np.conj(yf) / n

plt.plot(t,ymodulada)
plt.title("Señal modulada")
plt.show()

plt.plot(freq2[L],mag2[L])
plt.title("Espectro de frecuencia de la modulación DSB-SC")
plt.xlim(1.95,2.05)
plt.xlabel("Frecuencia $[kHz]$")
plt.show()

#filtro pasa altas para dejar solo una banda aka modulacion SSB
fc1 = fportadora

sos = signal.butter(200, fc1, 'hp', fs = fs, output = 'sos')
yfiltrado = signal.sosfilt(sos,ymodulada)
yffiltrado = np.fft.fft(yfiltrado,n)
mag3 = yffiltrado * np.conj(yffiltrado) / n

plt.plot(t,yfiltrado)
plt.title("Señal en el tiempo luego de ser filtrada")
plt.show()

plt.plot(freq2[L],mag3[L])
plt.title("SSB Espectro de frecuencia")
plt.xlim(1.95,2.05)
plt.xlabel("Frecuencia $[kHz]$")
plt.show()

#modulacion de nuevo, pero a 100k
fportadora2 = 200*fportadora
yportadora2 = np.cos(fportadora2 * 2 * np.pi * t)
ymodulada2 = yfiltrado * yportadora2

plt.plot(t,ymodulada2)
plt.title("Segunda modulación")
plt.show()

#filtro de nuevo
fc2 = fportadora2

sos2 = signal.butter(100, fc2, 'hp', fs = fs, output = 'sos')
yfiltrado2 = signal.sosfilt(sos2,ymodulada2)
yftransmitido = np.fft.fft(yfiltrado2,n)
mag4 = yftransmitido * np.conj(yffiltrado) / n

plt.plot(t,yfiltrado2)
plt.title("Señal modulada y filtrada para transmitir")
plt.show()

plt.plot(freq2[L],mag4[L])
plt.title("Segundo filtrado; Espectro de frecuencia")
plt.ylim(0,0.015)
plt.xlim(397,403)
plt.xlabel("Frecuencia $[kHz]$")
plt.show()


##demodulacion
