import numpy as np

#ehh, filtro pasa alta duh :v
def high_pass_filter(x,fc,dt):
    y = [0]*len(x)
    y[0]  = x[0]
    a = 1 / (2*np.pi*dt*fc + 1)
    n = len(x)
    for i in range(1,n):
        y[i] = a*y[i-1] + a*(x[i] - x[i - 1])
    return y

def gain(x,G):
    y = []
    for i in x:
        y.append(G*i)
    return y

def low_pass_filter(x,fc,dt):
    y = [0]*len(x)
    a = (2 * np.pi * dt * fc)/(2 * np.pi * dt * fc + 1)
    y[0] = a * x[0]
    n = len(x)
    for i in range(1,n):
        y[i] = a * x[i] + (1 - a) * y[i - 1]
    return y
