import numpy as np
from matplotlib import pyplot as plt
from scipy.fftpack import fft

fs=30000

f=500
T = 1.0 / f
t = np.linspace(0.0, 4*T, fs)
y = np.sin(f * 2.0*np.pi*t)

plt.plot(t,y)
plt.title("Señal Original")
plt.show()
fportadora=10000 #10k
yportadora=np.cos(fportadora*2*np.pi*t)
y1=y*yportadora

plt.plot(t,y1)
plt.title("Señal Modulada")
plt.show()

y1=np.ndarray.tolist(y1)
yr=np.random.normal(0,0.05,len(y1))
yr=np.ndarray.tolist(yr)
yd=[]
for i in range(len(y1)):
  yd.append(y1[i]+yr[i])
y1=np.array(yd)

plt.plot(t,y1)
plt.title("Señal recibida con ruido blanco")
plt.show()

#hasta aqui es solo la señal modulada con ruido
#plt.plot(t,y1)
#plt.show()
yf = fft(y1)
n=len(yf)
yf1=abs(yf)
ef = np.linspace(0.0, 1.0/(2.0*T), n//2) #espectro de frecuencia

ub=[]
ni=1
for i in range(n//2):
  if yf1[i]>ni:
    ub.append(i)

#plt.plot(ef,2.0*yf1[:n//2])
#plt.grid(True)
#plt.xlim(0,1200)
#plt.show()
#
#luego de esto comienza a demodular

ferrorportadora = []
for i in range(len(y1)):
    ferrorportadora.append(fportadora)
ferrorportadora = np.array(ferrorportadora)
ferrorportadora = np.ndarray.tolist(ferrorportadora)
errorf = np.random.normal(-0.05,0.05,len(y1))
fe = []
for i in range(len(y1)):
    fe.append(ferrorportadora[i] + errorf[i])
ferrorportadora = np.array(fe)
yportadora2 = np.cos(ferrorportadora*2*np.pi*t)
y2=y1*yportadora2
plt.plot(t,y2)
plt.title("demodulada con ruido blanco y error en la frecuencia")
plt.show()

yf2 = fft(y2)
n1=len(yf2)
yf2=abs(yf2)/n1
ub2=[]
ni=2
for i in range(n1//2):
  if yf2[i]>ni:
    ub2.append(i)

#plt.plot(ef,2.0*yf2[:n//2])
#plt.grid(True)
#plt.xlim(0,1200)
#plt.show()
#

#falta recuperar la señal

fcorte = 500
RC = 1/(2*np.pi*fcorte)
tau = 1/RC

ffiltro = (np.exp(-tau*t))/420

st = np.convolve(ffiltro,y2,mode='same')
tconvolve = np.linspace(0,4*T,len(st))

plt.plot(t,y,color = 'red',label='señal original')
plt.plot(tconvolve,st, color = 'blue',label = 'señal recuperada')
plt.title("Señal recuperada vs original")
plt.legend()
plt.xlim(0,0.004)
plt.show()
