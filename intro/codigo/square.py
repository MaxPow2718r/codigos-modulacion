import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

n = 100
t = np.linspace(-2*np.pi, 2*np.pi, 1000)
senal = 0
for i in range(1,n):
    bn = (2/np.pi)*((1/i)*(1 - (-1)**i))
    fourier = bn * np.sin(i*t)
    senal = fourier + senal
ft = signal.square(t)

plt.plot(t,ft, label="senal cuadrada", color='blue')
plt.plot(t,senal, label="serie de fourier", color='red')
plt.legend(loc='upper right')
plt.show()

